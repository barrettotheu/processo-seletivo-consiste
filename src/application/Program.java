package application;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import entities.Person;


public class Program {

    public static void main(String[] args) {

        String path = "dataset.txt";

        List<Person> list = new ArrayList<Person>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {

            String line = br.readLine();
            line = br.readLine();

            while (line != null) {

                String[] elements = line.split(";");

                if (elements.length < 4) {
                    line = br.readLine();
                }
                else {
                    String primeiroNome = elements[0];
                    String sobrenome = elements[1];
                    Double peso = Double.parseDouble(elements[2].replaceAll(",", "."));
                    Double altura = Double.parseDouble(elements[3].replaceAll("," , "."));

                    if (altura > peso){
                        peso = Double.parseDouble(elements[3].replaceAll(",", "."));
                        altura = Double.parseDouble(elements[2].replaceAll("," , "."));
                    }

                    Person prod = new Person(primeiroNome, sobrenome, peso, altura);
                    list.add(prod);

                    line = br.readLine();
                }

            }

            System.out.println("LISTA DE PESSOAS E SEUS RESPECTIVOS IMC's:");

            try (PrintWriter writer = new PrintWriter("LucasNeryMoreno.csv")) {

                StringBuilder sb = new StringBuilder();

                for (Person p : list) {
                    Double imc = p.getPeso()/(p.getAltura()*p.getAltura());
                    System.out.println(p.getPrimeiroNome() + " " + p.getSobrenome() + " " + String.format("%.2f", imc));
                    sb.append(p.getPrimeiroNome());
                    sb.append(' ');
                    sb.append(p.getSobrenome());
                    sb.append(' ');
                    sb.append(String.format("%.2f", imc));
                    sb.append("\n");
                }

                writer.write(sb.toString());

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            }

        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}