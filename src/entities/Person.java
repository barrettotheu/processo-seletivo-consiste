package entities;

import java.io.Serializable;
import java.util.Locale;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    private String primeiroNome;
    private String sobrenome;
    private Double peso;
    private Double altura;

    public Person() {
    }

    public Person(String primeiroNome, String sobrenome, Double peso, Double altura) {
        super(); // <<<<<<<<<<<<<<<<<<<<<<,
        this.primeiroNome = primeiroNome;
        this.sobrenome = sobrenome;
        this.peso = peso;
        this.altura = altura;
    }

    public String getPrimeiroNome() {
        return primeiroNome.toUpperCase(Locale.ROOT).replaceAll("\\s{2,}", " ").trim();
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getSobrenome() {
        return sobrenome.toUpperCase(Locale.ROOT).replaceAll("\\s{2,}", " ").trim();
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

}
